#!/usr/bin/env python3

import socket
import time
import struct
import click
import progressbar
from stable_conn import StableConnection

def exec_generic(conn, args):
    print(conn.sock.recv(4096).decode('utf-8'), end='')
    return True

def read_downloader_header(sock):
    available = sock.recv(1)
    if available == b'1':
        print("Not available")
        return None
    size = sock.recv(4)
    if len(size) != 4:
        print("Cannot read file size")
        return None
    return socket.ntohl(struct.unpack('I', size)[0])

def exec_download(conn, args):
    path = args[1]
    sock = conn.sock

    try:
        file = open(path, 'wb')
    except Exception:
        print("Cannot open", path)
        return False
    size = read_downloader_header(sock)
    if size is None: return True
    sock.send(b'\x00\x00\x00\x00')
    writed_size = 0
    with progressbar.ProgressBar(max_value=size) as bar:
        while writed_size < size:
            try:
                buf = sock.recv(4096)
            except socket.timeout:
                conn.reconnect()
                sock = conn.sock
                sock.send((" ".join(args) + '\n').encode('utf-8'))
                if read_downloader_header(sock) is None:
                    return True
                sock.send(struct.pack('I', socket.htonl(writed_size)))
                continue

            if not buf: break
            writed_size += len(buf)
            file.write(buf)
            bar.update(writed_size)
    print("Downloaded")
    return writed_size == size

def exec_close(conn, args):
    return False

executers = {
    "DOWNLOAD": exec_download,
    "CLOSE": exec_close
}

@click.command()
@click.argument('host', default='0.0.0.0')
@click.argument('port', default=8000)
def main(host, port):
    sock = socket.socket(socket.AF_INET,
                         socket.SOCK_STREAM,
                         socket.IPPROTO_TCP)
    sock.settimeout(30.0)
    sock.connect((host, port))

    print("Connected %s:%d..." % (host, port))

    conn = StableConnection(sock, (host, port))
    try:
        while True:
            line = input() + '\n'
            conn.sock.sendall(line.encode('utf-8'))
            args = line.split()
            if not args: continue
            success = executers.get(args[0], exec_generic)(conn, args)
            if not success: break
    except Exception as err:
        print("Error:", err)
    conn.sock.close()
    print("Connection closed")

if __name__ == '__main__':
    main()
