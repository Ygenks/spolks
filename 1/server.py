#!/usr/bin/env python3

import os
import struct
import socket
import datetime
import click
from stable_conn import StableConnection

def process_commands(conn):
    while True:
        line = conn.read_line()
        if line is None: continue
        args = line.split()
        if not args: continue
        success = executers.get(args[0], exec_invalid)(conn, args)
        if not success: break

def exec_invalid(conn, args):
    conn.sock.send("INVALID_COMMAND\n".encode('utf-8'))
    return True

def exec_echo(conn, args):
    buffer = " ".join(args[1:]).encode('utf-8') + b'\n'
    sended = conn.sock.send(buffer)
    return sended == len(buffer)

def exec_time(conn, args):
    timestr = str(datetime.datetime.now()).encode('utf-8') + b'\n'
    sended = conn.sock.send(timestr)
    return sended == len(timestr)

def exec_download(conn, args):
    path = args[1]
    sock = conn.sock
    try:
        size = os.path.getsize(path)
        file = open(path, 'rb')
    except Exception:
        conn.sock.send(b'1')
        return True
    sock.send(b'0')
    sock.send(struct.pack('I', socket.htonl(size)))

    seek = sock.recv(4)
    if len(seek) != 4:
        print("Cannot read file seek")
        return False
    seek = socket.ntohl(struct.unpack('I', seek)[0])
    file.seek(seek)
    size -= seek
    oldtimeout = sock.gettimeout()
    sock.settimeout(30)
    while size:
        buf = file.read(4096)
        if not buf: break
        size -= len(buf)
        try:
            sock.send(buf)
        except socket.timeout:
            print("timeout")
            return False
    sock.settimeout(oldtimeout)
    return size == 0

def exec_close(conn, args):
    return False

executers = {
    "ECHO": exec_echo,
    "TIME": exec_time,
    "DOWNLOAD": exec_download,
    "CLOSE": exec_close
}

@click.command()
@click.argument('host', default='0.0.0.0')
@click.argument('port', default=8000)
def main(host, port):
    serv_sock = socket.socket(socket.AF_INET,
                              socket.SOCK_STREAM,
                              socket.IPPROTO_TCP)
    serv_sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    serv_sock.bind((host, port))
    serv_sock.listen(5)
    print("Listening %s:%d..." % (host, port))

    while True:
        sock, addr = serv_sock.accept()
        print("New connection:", addr)
        conn = StableConnection(sock, addr)
        try:
            process_commands(conn)
        except Exception as err:
            print("Error:", err)
        conn.sock.close()
        print("Connection closed")


if __name__ == '__main__':
    main()
