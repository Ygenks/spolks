#!/usr/bin/env python3

import socket
import datetime
import click

class SocketReader():
    def __init__(self, socket):
        self.sock = socket
        self.stream = b""
    
    def read_line(self):
        while b'\n' not in self.stream:
            buff = self.sock.recv(1024)
            if not buff:
                raise Exception("Reading error")
            self.stream += buff
        i = self.stream.find(b'\n')
        res = self.stream[:i]
        self.stream = self.stream[i+1:]
        return res.decode('utf-8')

def process_commands(sock, addr):
    reader = SocketReader(sock)
    while True:
        line = reader.read_line()
        if line is None:
            print("line in None")
            break
        args = line.split()
        if not args:
            continue
        success = executers.get(args[0], exec_invalid)(sock, args)
        if not success:
            break

def exec_invalid(sock, args):
    # sock.send("INVALID_COMMAND".encode('utf-8'))
    return True

def exec_echo(sock, args):
    buffer = " ".join(args[1:]).encode('utf-8') + b'\n'
    size = sock.send(buffer)
    if size != len(buffer):
        print("TIME %d < %d" % (sended, size + 1))
        return False
    return True

def exec_time(sock, args):
    timestr = str(datetime.datetime.now()).encode('utf-8') + b'\n'
    sended = sock.send(timestr)
    if sended != len(timestr):
        print("TIME %d < %d" % (sended, size + 1))
        return False
    return True

def exec_close(sock, args):
    return False

executers = {
    "ECHO": exec_echo,
    "TIME": exec_time,
    "CLOSE": exec_close
}

@click.command()
@click.argument('host', default='0.0.0.0')
@click.argument('port', default=8000)
def main(host, port):
    serv_sock = socket.socket(socket.AF_INET,
                              socket.SOCK_STREAM,
                              socket.IPPROTO_TCP)
    serv_sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    serv_sock.bind((host, port))
    serv_sock.listen(5)
    print("Listening %s:%d..." % (host, port))
    
    while True:
        sock, addr = serv_sock.accept()
        print("New connection:", addr)
        # try:
        process_commands(sock, addr)
        sock.close()
        print("Connection closed")
        # except:
        #     print(msg)
        

if __name__ == '__main__':
    main()
