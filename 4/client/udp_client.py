#!/usr/bin/env python3

import socket
from time import sleep
import struct
import click
import progressbar
import hashlib

sock = None


def generic(args, addr):
    line = ' '.join(args) + '\n'
    sock.sendto(line.encode('utf-8'), addr)
    msg, ad = sock.recvfrom(4096)
    if ad != addr:
        print('Message from unknown sender', ad)
    print(msg.decode('utf-8'), end='')


def download(args, addr):
    sock.sendto(b'download-meta ' + args[1].encode('utf-8'), addr)
    res_data, res_addr = sock.recvfrom(4096)
    if res_addr != addr:
        print("Wrong sender")
        return
    if res_data == b'not found':
        print("File not found")
        return
    if len(res_data) != 4:
        print("Initial error")
        return

    size = socket.ntohl(struct.unpack('I', res_data)[0])
    seq_num = 0
    f = open(args[1], 'wb')
    attempt = 0
    with progressbar.ProgressBar(max_value=size) as bar:
        while seq_num < size:
            sock.sendto((' '.join(args) + ' ' + str(seq_num)).encode('utf-8'), addr)

            try:
                message, res_addr = sock.recvfrom(49 * 1024)
            except socket.timeout:
                attempt += 1
                if attempt < 30:
                    continue
                print('\n\nTimeout of timeouts\n')
                break

            if res_addr != addr:
                print('Wrong sender')
            sha1 = message[:20]
            real_sha1 = hashlib.sha1(message[20:]).digest()
            if real_sha1 != sha1:
                # print('SHA-1 mismatch')
                continue
            real_seq_num = socket.ntohl(struct.unpack('I', message[20:24])[0])
            if real_seq_num != seq_num:
                # print('Sequence number mismatch')
                continue
            data = message[24:]
            f.write(data)
            seq_num += len(data)
            bar.update(seq_num)
            attempt = 0
    f.close()


executers = {
    "download": download,
}

@click.group()
def cli():
    pass


@cli.command()
@click.argument('host', default='127.0.0.1')
@click.argument('port', default=8000)
def run(host, port):
    global sock
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sock.settimeout(30)
    serv_addr = (host, port)

    print("Connected %s:%d..." % (host, port))

    while True:
        try:
            line = input() + '\n'
            args = line.split()
            if not args:
                continue
            executers.get(args[0], generic)(args, serv_addr)
        except Exception as err:
            print("Error:", err)
    sock.close()
    print("Connection closed")


if __name__ == '__main__':
    cli()
