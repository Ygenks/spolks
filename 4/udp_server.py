#!/usr/bin/env python3

import os
import struct
import socket
import datetime
import hashlib
import click
import multiprocessing as mp

serv_sock = None
files_cache = {}


def process_command(message, addr):
    args = message.decode('utf-8').split()
    if not args:
        return

    executers.get(args[0], invalid)(args, addr)


def invalid(args, addr):
    serv_sock.sendto("INVALID_COMMAND\n".encode('utf-8'), addr)


def time_meta(args, addr):
    process = mp.Process(target=time, args=(args, addr))
    process.start()
    print('Process {} started'.format(process.pid))


def echo_meta(args, addr):
    process = mp.Process(target=echo, args=(args, addr))
    process.start()
    print('Process {} started'.format(process.pid))


def echo(args, addr):
    buffer = " ".join(args[1:]).encode('utf-8') + b'\n'
    serv_sock.sendto(buffer, addr)
    print('Process {} finished'.format(os.getpid()))


def time(args, addr):
    timestr = str(datetime.datetime.now()).encode('utf-8') + b'\n'
    serv_sock.sendto(timestr, addr)
    print('Process {} finished'.format(os.getpid()))


def get_file(path):
    if path in files_cache:
        f = files_cache[path]
    else:
        try:
            size = os.path.getsize(path)
            f = {'file': open(path, 'rb'), 'size': size}
        except (IOError, OSError) as e:
            return None
        files_cache[path] = f
    return f


def download_meta(args, addr):
    file = get_file(args[1])
    if file is None:
        serv_sock.sendto(b'not found', addr)

    serv_sock.sendto(struct.pack('I', socket.htonl(file['size'])), addr)

    process = mp.Process(target=download, args=(args, addr))
    process.start()

    print('Process {} started'.format(process.pid))


def download(args, addr):

    while True:
        try:
            message, recv_addr = serv_sock.recvfrom(4096)
        except socket.timeout:
            break

        #FIXME
        if recv_addr != addr:
            continue

        _, path, seek = message.decode('utf-8').split(' ')
        file_cache = get_file(path)
        f = file_cache['file']

        seek = f.seek(int(seek))
        data = f.read(4096)
        buf = struct.pack('I', socket.htonl(seek)) + data
        sha = hashlib.sha1(buf).digest()
        serv_sock.sendto(sha + buf, addr)
        if seek + len(data) == file_cache['size']:
            f.close()
            del files_cache[path]
            break

    print('Process {} finished'.format(os.getpid()))


executers = {
    "echo": echo_meta,
    "time": time_meta,
    "download-meta": download_meta,
}


@click.group()
def cli():
    pass


@cli.command()
@click.argument('host', default='0.0.0.0')
@click.argument('port', default=8000)
def run(host, port):
    global serv_sock
    serv_sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    serv_sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    serv_sock.bind((host, port))

    print("Listening %s:%d..." % (host, port))

    oldtimeout = serv_sock.gettimeout()
    while True:
        serv_sock.settimeout(oldtimeout)
        message, addr = serv_sock.recvfrom(4096)
        serv_sock.settimeout(2)
        try:
            process_command(message, addr)
        except Exception as err:
            print("Error:", err)


if __name__ == '__main__':
    cli()
