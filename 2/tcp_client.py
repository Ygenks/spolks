#!/usr/bin/env python3

import socket
import struct
import os
import click
import progressbar
from connection import Connection

socket_g = None


def execute_command(conn, args):
    print(conn.sock.recv(4096).decode('utf-8'), end='')
    return True


def read_downloading_header(sock):

    available = sock.recv(1)

    if available == b'1':
        print("Not available")
        return None

    size = sock.recv(4)

    if len(size) != 4:
        print("Cannot read file size")
        return None

    return socket.ntohl(struct.unpack('I', size)[0])


def download(conn, args):

    global socket_g

    path = args[1]
    sock = conn.sock

    try:
        file = open(path, 'wb')
    except Exception:
        print('Cannot open', path)
        return False

    size = read_downloading_header(sock)

    if size is None:
        return True

    sock.send(b'\x00\x00\x00\x00')

    written_size = 0

    socket_g = sock

    WR_MD = 5 * 2**20

    with progressbar.ProgressBar(max_value=size) as bar:

        while written_size < size:
            try:

                buffer = sock.recv(4096)

                if written_size // WR_MD < (
                        written_size + len(buffer)) // WR_MD:

                    data = socket_g.recv(1, socket.MSG_OOB)
                    print('\nOOB message:', repr(data))

            except socket.timeout:

                conn.reconnect()

                sock = conn.sock
                socket_g = sock

                # fcntl.fcntl(sock.fileno(), fcntl.F_SETOWN, os.getpid())

                sock.send((" ".join(args) + '\n').encode('utf-8'))

                if read_downloading_header(sock) is None:
                    return True

                sock.send(struct.pack('I', socket.htonl(written_size)))

                continue

            if not buffer:
                break

            written_size += len(buffer)
            file.write(buffer)
            bar.update(written_size)

    print('Downloaded')
    return written_size == size


def close(conn, args):
    return False


executers = {'download': download, 'close': close}


@click.group()
def cli():
    pass


@cli.command()
@click.argument('host', default='0.0.0.0')
@click.argument('port', default=8000)
def run(host, port):
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM,
                         socket.IPPROTO_TCP)

    sock.settimeout(30.0)
    sock.connect((host, port))

    print('Connected to {}:{}'.format(host, port))

    conn = Connection(sock, (host, port))

    try:
        while True:
            line = input() + '\n'
            conn.sock.sendall(line.encode('utf-8'))
            args = line.split()

            if not args:
                continue

            success = executers.get(args[0], execute_command)(conn, args)

            if not success:
                break

    except Exception as e:
        print('Error:', e)

    conn.sock.close()
    print('Connection closed')


if __name__ == '__main__':
    cli()
