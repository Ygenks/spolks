#!/usr/bin/env python3

import os
import struct
import socket
import datetime
import click
from time import sleep
from connection import Connection


def process_commands(conn):
    while True:
        line = conn.readline()

        if line is None:
            continue

        args = line.split()

        if not args:
            continue

        success = executers.get(args[0], invalid_command)(conn, args)

        if not success:
            break


def invalid_command(conn, args):
    conn.sock.send("INVALID_COMMAND\n".encode('utf-8'))
    return True


def echo(conn, args):
    buffer = " ".join(args[1:]).encode('utf-8') + b'\n'

    sended = conn.sock.send(buffer)

    return sended == len(buffer)


def time(conn, args):
    current_time = str(datetime.datetime.now()).encode('utf-8') + b'\n'

    sent = conn.sock.send(current_time)

    return sent == len(current_time)


def download(conn, args):
    path = args[1]
    sock = conn.sock

    try:
        size = os.path.getsize(path)
        file = open(path, 'rb')
    except Exception:
        conn.sock.send(b'1')
        return True

    sock.send(b'0')
    sock.send(struct.pack('I', socket.htonl(size)))

    seek = sock.recv(4)

    if len(seek) != 4:
        print("Cannot read file size")
        return False

    seek = socket.ntohl(struct.unpack('I', seek)[0])

    file.seek(seek)
    size -= seek

    oldtimeout = sock.gettimeout()
    sock.settimeout(30)

    written = 0
    WR_MD = 5 * 2**20

    while size:

        buffer = file.read(4096)

        if not buffer:
            break

        size -= len(buffer)

        try:
            sock.send(buffer)

            if written // WR_MD < (written + len(buffer)) // WR_MD:

                sleep(0.05)
                sock.send(b'!', socket.MSG_OOB)

            written += len(buffer)

        except socket.timeout:
            print("timeout")
            return False

    sock.settimeout(oldtimeout)
    return size == 0


def close(conn, args):
    return False


executers = {"echo": echo, "time": time, "download": download, "close": close}


@click.group()
def cli():
    pass


@cli.command()
@click.argument('host', default='0.0.0.0')
@click.argument('port', default=8000)
def run(host, port):
    serv_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM,
                              socket.IPPROTO_TCP)

    serv_sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    serv_sock.bind((host, port))
    serv_sock.listen(5)

    print('Listening to {}:{}'.format(host, port))

    while True:

        sock, addr = serv_sock.accept()

        print('{} connected'.format(addr))

        conn = Connection(sock, addr)

        try:
            process_commands(conn)

        except Exception as e:
            print('Error:', e)

        conn.sock.close()

        print('Connection closed')


if __name__ == '__main__':
    cli()
