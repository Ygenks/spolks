import socket

class Connection():
    def __init__(self, socket, addr):
        self.sock = socket
        self.addr = addr
        self.stream = b""

    def readline(self):
        while b'\n' not in self.stream:
            buff = self.sock.recv(1024)
            if not buff:
                raise Exception("Reading error")
            self.stream += buff
        i = self.stream.find(b'\n')
        res = self.stream[:i]
        self.stream = self.stream[i+1:]
        try:
            line = res.decode('utf-8')
        except UnicodeDecodeError:
            return None
        return line

    def reconnect(self):
        self.sock.close()
        sock = socket.socket(socket.AF_INET,
                            socket.SOCK_STREAM,
                            socket.IPPROTO_TCP)
        sock.settimeout(30.0)
        sock.connect(self.addr)
        self.sock = sock
