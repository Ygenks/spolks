import socket


class Connection():
    def __init__(self, sock, addr):
        self.sock = sock
        self.addr = addr
        self.stream = b''

    def readline(self):
        while b'\n' not in self.stream:
            buffer = self.sock.recv(1024)
            if not buffer:
                raise Exception('Read error')
            self.stream += buffer
        trailer = self.stream.find(b'\n')
        data = self.stream[:trailer]
        self.stream = self.stream[trailer + 1:]
        try:
            line = data.decode('utf-8')
        except UnicodeDecodeError:
            return None
        return line

    def reconnect(self):
        self.sock.close()
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM,
                             socket.IPPROTO_TCP)
        sock.settimeout(30.0)
        sock.connect(self.addr)
        self.sock = sock
