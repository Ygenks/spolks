#!/usr/bin/env python3

import socket
import struct
import click
import progressbar
from connection import Connection


def execute_command(conn, args):
    print(conn.sock.recv(4096).decode('utf-8'), end='')
    return True


def read_downloading_header(sock):

    available = sock.recv(1)

    if available == b'1':
        print('Not available')
        return None

    size = sock.recv(4)

    if len(size) != 4:
        print('Cannot read file size')
        return None

    return socket.ntohl(struct.unpack('I', size)[0])


def download(conn, args):

    filepath = args[1]
    sock = conn.sock

    try:
        file = open(filepath, 'wb')
    except Exception:
        print("Cannot open", filepath)
        return False

    size = read_downloading_header(sock)

    if size is None:
        return True

    written_size = 0

    with progressbar.ProgressBar(max_value=size, widgets=[progressbar.SimpleProgress()]) as bar:

        while written_size < size:

            try:
                buffer = sock.recv(10 * 1024)
            except socket.timeout:

                conn.reconnect()

                sock = conn.sock

                sock.send((" ".join(args) + ' ' + str(written_size) +
                           '\n').encode('utf-8'))

                if read_downloading_header(sock) is None:
                    return True

                continue

            if not buffer:
                break

            written_size += len(buffer)


            file.write(buffer)

            bar.update(written_size)

    print("Downloaded")

    return written_size == size


def close(conn, args):
    return False


executers = {"download": download, "close": close}


@click.group()
def cli():
    pass

@cli.command()
@click.argument('host', default='0.0.0.0')
@click.argument('port', default=8000)
def run(host, port):

    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM,
                         socket.IPPROTO_TCP)

    sock.settimeout(30.0)

    sock.connect((host, port))

    print('Connected to {}:{}'.format(host, port))

    conn = Connection(sock, (host, port))

    try:

        while True:
            line = input() + '\n'

            conn.sock.sendall(line.encode('utf-8'))
            args = line.split()

            if not args:
                continue

            success = executers.get(args[0], execute_command)(conn, args)

            if not success:
                break

    except Exception as e:

        print('Error:', e)

    conn.sock.close()

    print('Connection closed')


if __name__ == '__main__':
    cli()
