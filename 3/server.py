#!/usr/bin/env python3

import os
import struct
import datetime
import socket
import select
import click

files = {}

def get_file(path):

    if path in files:

        file = files[path]
    else:
        try:
            size = os.path.getsize(path)
            file = {'file': open(path, 'rb'), 'size': size}
        except:
            return None

        files[path] = file

    return file


class ConnectionInfo():
    def __init__(self, connection):
        self.command = None
        self.written = 0
        self.size = 0
        self.conn = connection


def invalid_command(conn):
    conn.conn.send("Invalid command\n".encode('utf-8'))
    return True, True


def echo(conn):

    buffer = " ".join(conn.command[1:]).encode('utf-8') + b'\n'

    sended = conn.conn.send(buffer)

    return sended == len(buffer), True


def time(conn):

    current_time = str(datetime.datetime.now()).encode('utf-8') + b'\n'

    sended = conn.conn.send(current_time)

    return sended == len(current_time), True


def close(conn):
    return False, True


def download(conn):
    file = get_file(conn.command[1])

    if file is None:
        conn.conn.send(b'1')
        return True, False

    size = file['size']

    if conn.size == 0:
        conn.size = size

        if len(conn.command) < 3:
            conn.written = 0
        else:
            conn.written = int(conn.command[2])

        conn.conn.send(b'0')
        conn.conn.send(struct.pack('I', socket.htonl(size)))

        return True, False

    seek = conn.written

    file = file['file']
    file.seek(seek)

    buf = file.read(4096)

    if not buf:
        return False, False

    conn.conn.send(buf)
    conn.written += len(buf)
    end = False

    if conn.written == conn.size:
        file.close()
        del files[conn.command[1]]
        end = True

    return True, end

executers = {
    "echo": echo,
    "time": time,
    "download": download,
    "close": close
}

@click.group()
def cli():
    pass

@cli.command()
@click.argument('host', default='0.0.0.0')
@click.argument('port', default=8000)
def run(host, port):
    server = socket.socket(socket.AF_INET, socket.SOCK_STREAM,
                           socket.IPPROTO_TCP)

    server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

    server.bind((host, port))
    server.listen(5)

    print('Listening %s:%d' % (host, port))

    inputs = [server]
    outputs = []
    connections = {}

    while inputs:

        readable, writable, exceptional = select.select(inputs, outputs, inputs)

        for sock in readable:

            if sock is server:
                conn, addr = sock.accept()
                conn.setblocking(0)

                inputs.append(conn)
                connections[conn] = ConnectionInfo(conn)

                print('New connection:', addr)

            else:

                data = sock.recv(4096)

                if data:

                    conn = connections[sock]
                    conn.command = data.decode('utf-8').split()
                    conn.written = 0
                    conn.size = 0

                    if sock not in outputs:
                        outputs.append(sock)

                else:

                    if sock in outputs:
                        outputs.remove(sock)

                    inputs.remove(sock)

                    sock.close()
                    del connections[sock]

        for sock in writable:
            conn = connections.get(sock)

            if not conn:
                continue

            success, end = executers.get(conn.command[0], invalid_command)(conn)

            if end:

                outputs.remove(sock)

            if not success:
                sock.close()
                del connections[sock]

        for sock in exceptional:

            inputs.remove(sock)

            if sock in outputs:

                outputs.remove(sock)

            sock.close()
            del connections[sock]


if __name__ == '__main__':
    cli()
